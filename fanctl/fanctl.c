#include <bcm2835.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <math.h>
#include <string.h>

void setupPWM();
void terminateHandler(int signal);

// Use PWM Channel 1 and Pin 33 (GPIO 13)
const uint8_t Channel = 1;
const uint8_t Pin = RPI_V2_GPIO_P1_33;

// This controls the max range of the PWM signal
const uint32_t Range = 1024;

const float SlowdownIncrement = 0.1;
const float DesiredSpeed = 0.6;
float currentSpeed = 1;

int main() {
    if (!bcm2835_init()) {
        printf("Could not initialise bcm2835, exiting\n");
        return 1;
    }

    setupPWM();

    printf("Setup complete, starting...\n");

    // start at full speed
    bcm2835_pwm_set_data(Channel, Range);
    printf("Started at full speed\n");
    sleep(3);

    // drop the speed down to the target
    while(currentSpeed >= DesiredSpeed) {
        currentSpeed -= SlowdownIncrement;

        printf("Reducing to %.0f%%\n", ceil(currentSpeed * 100));

        bcm2835_pwm_set_data(Channel, Range * currentSpeed);
        sleep(1);
    }

    bcm2835_close();

    printf("Reached target speed\n");

    signal(SIGTERM, terminateHandler);

    fflush(stdout);

    pause();
    printf("Closing\n");
}

void setupPWM() {
    // set pin and alt function (alt 0 in this case)
    bcm2835_gpio_fsel(Pin, BCM2835_GPIO_FSEL_ALT0);

    // set prescaler (19.2MHz base / 128 = 150KHz)
    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_128);

    // Set PWM range (150KHz / 1024 = 146Hz)
    bcm2835_pwm_set_range(Channel, Range);

    // set mode on PWN channel 1 to 'Markspace' and enabled
    bcm2835_pwm_set_mode(Channel, 1, 1);
}

void terminateHandler(int signal) {
    printf("Received %s\n", strsignal(signal));

    if (!bcm2835_init()) {
        printf("Could not reinitialise to reset PWM in response to SIGTERM\n");
        return;
    }

    bcm2835_pwm_set_mode(Channel, 1, 0);
    bcm2835_close();

    printf("PWM Reset complete.\n");
}
