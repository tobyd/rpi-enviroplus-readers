# EnviroPlus utilities for Pi

## Dependencies
Compile and build the [BCM2835 library](https://www.airspayce.com/mikem/bcm2835/). 
- Install the C compiler `sudo apt install gcc`
- Download the latest version of the BCM library
- unpack it in its directory and execute the following commands to install it

```
    ./configure
    make
    sudo make install
```
The library should then be available in the `/usr/local/lib` folder and can be linked.

### FanCtl
Fanctl is a simple utility to run a small 5v fan via PWM from the pi to encourage the EnviroPlus to be a little
more accurate. Should be installed as a simple `systemd` service using the supplied script with the output binary.
To run the fan, bend [GPIO pin 13](https://pinout.xyz/pinout/pin33_gpio13#) to allow you to access it with the EnviroPlus
board connected (the board doesn't use this pin). Create a simple transistor PWM board to do the motor control - see
circuit diagram in repo. 

### Parti
Interface to the PMS5003 Particulate Sensor on the EnviroPlus board. Once you've compiled and installed the BCM library
just run the build script.