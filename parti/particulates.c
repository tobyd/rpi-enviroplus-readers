#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <bcm2835.h>
#include <signal.h>
#include <errno.h>

extern int errno;

// structs
struct Reading
{
    uint16_t framelength;
    uint16_t std_pm1_0;
    uint16_t std_pm2_5;
    uint16_t std_pm10_0;
    uint16_t atmospheric_pm1_0;
    uint16_t atmospheric_pm1_5;
    uint16_t atmospheric_pm10_0;
    uint16_t particles_0_3;
    uint16_t particles_0_5;
    uint16_t particles_1_3;
    uint16_t particles_2_5;
    uint16_t particles_5_0;
    uint16_t particles_10_0;
    uint16_t reservedFn;
    uint16_t checksum;
}
reading;

// main flow functions
bool setup();
void readWriteLoop();
void cleanUp();

// particulates device functions
bool setupParticulatesDevice();
void cleanUpParticulatesDevice();
bool waitForFrameStart();
bool takeReading();

// utility functions
bool validateChecksum();
void printReading();

// signal handlers
void terminateHandler(int signal);

// consts
const uint_fast32_t enablePin = RPI_V2_GPIO_P1_13, resetPin = RPI_V2_GPIO_P1_15;
const uint_fast16_t readingSize = 30;	// a full reading is 15 x 2byte chunks
const uint_fast8_t readIntervalSeconds = 5;
const uint8_t frameStartOne = 0x42, frameStartTwo = 0x4d;

// members
int_fast32_t serialHandle = -1;
struct termios previousPortConfig, desiredPortConfig;
uint8_t dataBuffer[30];
bool stop = false;

int main()
{
    if (setup() && setupParticulatesDevice())
    {
        readWriteLoop();
        cleanUp();
        return 0;
    }

    return 1;
}

void terminateHandler(int signal)
{
    printf("Received %s signal\n", strsignal(signal));
    stop = true;
}

bool setup()
{
    signal(SIGTERM, terminateHandler);
    signal(SIGINT, terminateHandler);

    serialHandle = open("/dev/serial0", O_RDWR);

    if (serialHandle == -1)
    {
        printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
        return false;
    }

    tcgetattr(serialHandle, &previousPortConfig);

    memset(&desiredPortConfig, 0, sizeof(desiredPortConfig));

   	// 9600 baud, 8bit, no parity, 1 stopbit
    desiredPortConfig.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    desiredPortConfig.c_iflag = IGNPAR;	// ignore parity errors
    desiredPortConfig.c_oflag = 0;	// raw output
    desiredPortConfig.c_lflag = 0;	// disable cannonical mode

    desiredPortConfig.c_cc[VTIME] = 30;
    desiredPortConfig.c_cc[VMIN] = 0;

    tcflush(serialHandle, TCIFLUSH);
    tcsetattr(serialHandle, TCSANOW, &desiredPortConfig);

    return true;
}

void readWriteLoop()
{
    while (stop == false)
    {
        if (waitForFrameStart() && takeReading())
        {
           	// do something with reading, write it to DB, post it to somewhere, whatever
        }

        for (uint_fast8_t waitForTimeoutCheck = 0; waitForTimeoutCheck < readIntervalSeconds; waitForTimeoutCheck++)
        {
            if (stop)
            {
                printf("Received stop signal, stopping.\n");
                break;
            }
            else
            {
                sleep(1);
            }
        }
    }

    printf("Main read-loop stopped.\n");
}

bool waitForFrameStart()
{
    size_t res;

    for (uint_fast32_t i = 0; i < readingSize; i++)
    {
        res = read(serialHandle, dataBuffer, 1);

        if (res == 1 && dataBuffer[0] == frameStartOne)
        {
            res = read(serialHandle, dataBuffer, 1);

            if (res == 1 && dataBuffer[0] == frameStartTwo)
            {
                return true;
            }
        }
    }

    if (res == -1)
    {
        printf("Encountered error waiting for a frame start condition %s\n", strerror(errno));
    }
    else
    {
        printf("Did not receive a start sequence in %i bytes\n", readingSize);
    }

    return false;
}

bool takeReading()
{
   	// read all bytes into buffer (15 x 2byte chunks = 30 bytes)
    size_t res = read(serialHandle, &dataBuffer, 30);

    if (res == -1)
    {
        printf("Encountered error reading frame %s\n", strerror(errno));
        return false;
    }
    else
    {
       	// swap the values around as they arrive reversed, whilst making a checksum
        uint8_t temp = 0, halfBufferSize = sizeof(dataBuffer) / 2, pairIndex = 0;

        for (uint_fast8_t i = 0; i < halfBufferSize; i++)
        {
            pairIndex = i * 2;
            temp = dataBuffer[pairIndex];
            dataBuffer[pairIndex] = dataBuffer[pairIndex + 1];
            dataBuffer[pairIndex + 1] = temp;
        }

        memcpy(&reading, dataBuffer, sizeof(dataBuffer));

        return validateChecksum();
    }
}

void cleanUp()
{
    tcsetattr(serialHandle, TCSANOW, &previousPortConfig);
    cleanUpParticulatesDevice();
}

bool setupParticulatesDevice()
{
    if (!bcm2835_init())
    {
        printf("Unable to initialise the particulates device, do you have the correct permissions?");
        return false;
    }

    bcm2835_gpio_fsel(enablePin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(resetPin, BCM2835_GPIO_FSEL_OUTP);

    bcm2835_gpio_write(enablePin, HIGH);
    bcm2835_gpio_write(resetPin, HIGH);

    bcm2835_close();

    sleep(1);	// let device stabilise

    return true;
}

void cleanUpParticulatesDevice()
{
    if (!bcm2835_init())
    {
        printf("Unable to properly clean up the state of the particulates device");
        return;
    }

    bcm2835_gpio_write(enablePin, LOW);
    bcm2835_gpio_write(resetPin, LOW);

    bcm2835_gpio_set_pud(enablePin, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(resetPin, BCM2835_GPIO_PUD_DOWN);

    bcm2835_gpio_fsel(enablePin, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(resetPin, BCM2835_GPIO_FSEL_INPT);

    bcm2835_close();
}

bool validateChecksum()
{
    uint16_t checksum = frameStartOne + frameStartTwo;

    for (uint8_t i = 0; i < sizeof(dataBuffer) - 2; i++)
    {
        checksum += dataBuffer[i];
    }

    if (checksum != reading.checksum)
    {
        printf("Checksum failure. Calculated %i, expecting %i\n", checksum, reading.checksum);
        return false;
    }

    return true;
}

void printReading()
{
    printf("framelength: %d\n", reading.framelength);
    printf("std_pm1_0: %d\n", reading.std_pm1_0);
    printf("std_pm2_5: %d\n", reading.std_pm2_5);
    printf("std_pm10_0: %d\n", reading.std_pm10_0);
    printf("atmospheric_pm1_0: %d\n", reading.atmospheric_pm1_0);
    printf("atmospheric_pm1_5: %d\n", reading.atmospheric_pm1_5);
    printf("atmospheric_pm10_0: %d\n", reading.atmospheric_pm10_0);
    printf("particles_0_3: %d\n", reading.particles_0_3);
    printf("particles_0_5: %d\n", reading.particles_0_5);
    printf("particles_1_3: %d\n", reading.particles_1_3);
    printf("particles_2_5: %d\n", reading.particles_2_5);
    printf("particles_5_0: %d\n", reading.particles_5_0);
    printf("particles_10_0: %d\n", reading.particles_10_0);
    printf("reservedFn: %d\n", reading.reservedFn);
    printf("checksum: %d\n", reading.checksum);
}